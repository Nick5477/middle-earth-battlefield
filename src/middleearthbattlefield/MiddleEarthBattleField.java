/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;
import middleearthbattlefield.middleearth.armies.Army;
import middleearthbattlefield.middleearth.armies.DarkArmy;
import middleearthbattlefield.middleearth.armies.LightArmy;
import middleearthbattlefield.middleearth.entities.MiddleEarthCitizen;

/**
 *
 * @author User
 */
public class MiddleEarthBattleField {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // TODO code application logic here        
        
        ArrayList<MiddleEarthCitizen> wonArmy = loadWonArmyState();
        System.out.println("Last won army:");
        printArmy(wonArmy);
        
        LightArmy lightArmy = new LightArmy();
        DarkArmy darkArmy = new DarkArmy();
        
        startNewBattle(lightArmy, darkArmy);
    }
    
    public static void startNewBattle(LightArmy lightArmy, DarkArmy darkArmy) throws IOException{
        //1 раунд
        int i;
        int j;
        System.out.println("Light army:");
        printArmy(lightArmy);
        System.out.println("Dark army:");
        printArmy(darkArmy);
        System.out.println("First round begins!!!");
        while (!lightArmy.isRidersDead() && !darkArmy.isRidersDead()){
            i = randomizeIndex(lightArmy.getRiders());
            j = randomizeIndex(darkArmy.getRiders());
            
            if (isLightArmyFirst()){
                System.out.println("Light army attacks!");
                System.out.println("Attacker: " + lightArmy.getRiders().get(i).toString());
                System.out.println("BEFORE STRIKE: " + darkArmy.getRiders().get(j).toString());
                
                boolean isDead = darkArmy.attackRider(j, lightArmy.getRiders().get(i).getPower());
                
                System.out.println("AFTER STRIKE: " + darkArmy.getRiders().get(j).toString());
                
                if (!isDead){
                    System.out.println("Dark army REVENGE!");
                    System.out.println("Attacker: " + darkArmy.getRiders().get(j).toString());
                    System.out.println("BEFORE STRIKE: " + lightArmy.getRiders().get(i).toString());
                    
                    lightArmy.attackRider(i, darkArmy.getRiders().get(j).getPower());
                    
                    System.out.println("AFTER STRIKE: " + lightArmy.getRiders().get(i).toString());
                }                
            } else {
                System.out.println("Dark army attacks!");
                System.out.println("Attacker: " + darkArmy.getRiders().get(j).toString());
                System.out.println("BEFORE STRIKE: " + lightArmy.getRiders().get(i).toString());
                
                boolean isDead = lightArmy.attackRider(i, darkArmy.getRiders().get(j).getPower());
                
                System.out.println("AFTER STRIKE: " + lightArmy.getRiders().get(i).toString());
                
                if (!isDead){
                    System.out.println("Light army REVENGE!");
                    System.out.println("Attacker: " + lightArmy.getRiders().get(i).toString());
                    System.out.println("BEFORE STRIKE: " + darkArmy.getRiders().get(j).toString());
                    
                    darkArmy.attackRider(j, lightArmy.getRiders().get(i).getPower());
                    
                    System.out.println("AFTER STRIKE: " + darkArmy.getRiders().get(j).toString());
                }
            }
            
            System.out.println();
        }
        
        System.out.println("Light army after first round:");
        printArmy(lightArmy);
        System.out.println("Dark army after first round:");
        printArmy(darkArmy);
        if (lightArmy.isRidersDead()){
            System.out.println("In first round: DARK ARMY WINS");
        } else {
            System.out.println("In first round: LIGHT ARMY WINS");
        }
        
        // 2 раунд
        System.out.println("Second round begins!!!");
        
        while (!lightArmy.isInfantrymenDead() && !darkArmy.isInfantrymenDead()){
            i = randomizeIndex(lightArmy.getInfantrymen());
            j = randomizeIndex(darkArmy.getInfantrymen());
            
            if (isLightArmyFirst()){
                System.out.println("Light army attacks!");
                System.out.println("Attacker: " + lightArmy.getInfantrymen().get(i).toString());
                System.out.println("BEFORE STRIKE: " + darkArmy.getInfantrymen().get(j).toString());
                
                boolean isDead = darkArmy.attackInfantrymen(j, lightArmy.getInfantrymen().get(i).getPower());
                
                System.out.println("AFTER STRIKE: " + darkArmy.getInfantrymen().get(j).toString());
                
                if (!isDead){
                    System.out.println("Dark army REVENGE!");
                    System.out.println("Attacker: " + darkArmy.getInfantrymen().get(j).toString());
                    System.out.println("BEFORE STRIKE: " + lightArmy.getInfantrymen().get(i).toString());
                    
                    lightArmy.attackInfantrymen(i, darkArmy.getInfantrymen().get(j).getPower());
                    
                    System.out.println("AFTER STRIKE: " + lightArmy.getInfantrymen().get(i).toString());
                }
                
            } else {
                System.out.println("Dark army attacks!");
                System.out.println("Attacker: " + darkArmy.getInfantrymen().get(j).toString());
                System.out.println("BEFORE STRIKE: " + lightArmy.getInfantrymen().get(i).toString());
                 
                boolean isDead = lightArmy.attackInfantrymen(i, darkArmy.getInfantrymen().get(j).getPower());
                   
                System.out.println("AFTER STRIKE: " + lightArmy.getInfantrymen().get(i).toString());
                    
                if (!isDead){
                    System.out.println("Light army REVENGE!");
                    System.out.println("Attacker: " + lightArmy.getInfantrymen().get(i).toString());
                    System.out.println("BEFORE STRIKE: " + darkArmy.getInfantrymen().get(j).toString());
                       
                    darkArmy.attackInfantrymen(j, lightArmy.getInfantrymen().get(i).getPower());
                       
                    System.out.println("AFTER STRIKE: " + darkArmy.getInfantrymen().get(j).toString());
                }
            }
            
            System.out.println();
        }
        
        System.out.println("Light army after second round:");
        printArmy(lightArmy);
        System.out.println("Dark army after second round:");
        printArmy(darkArmy);
        if (lightArmy.isInfantrymenDead()){
            System.out.println("In second round: DARK ARMY WINS");
        } else {
            System.out.println("In second round: LIGHT ARMY WINS");
        }
        
        //3 раунд
        
        if (lightArmy.isDead()){
            System.out.println("In final: DARK ARMY WINS");
            darkArmy.uniteAllCitizens();
            saveWonArmyState(darkArmy.getAll());
            return;
        }
        if (darkArmy.isDead()){
            System.out.println("In final: LIGHT ARMY WINS");
            lightArmy.uniteAllCitizens();
            saveWonArmyState(lightArmy.getAll());
            return;
        }
        lightArmy.uniteAllCitizens();
        darkArmy.uniteAllCitizens();
        
        System.out.println("Third round begins!!!");
        while (!lightArmy.isDead() && !darkArmy.isDead()){
            i = randomizeIndex(lightArmy.getAll());
            j = randomizeIndex(darkArmy.getAll());
            
            boolean lightArmyFirst = lightArmy.getAll().get(i).firstStrike();
            boolean darkArmyFirst = darkArmy.getAll().get(j).firstStrike();
            if (lightArmyFirst && darkArmyFirst
                || (!lightArmyFirst && !darkArmyFirst)){
                lightArmyFirst = isLightArmyFirst();
            }         
            
            if (lightArmyFirst){
                System.out.println("Light army attacks!");
                System.out.println("Attacker: " + lightArmy.getAll().get(i).toString());
                System.out.println("BEFORE STRIKE: " + darkArmy.getAll().get(j).toString());
                
                boolean isDead = darkArmy.attackCitizen(j, lightArmy.getAll().get(i).getPower());
                
                System.out.println("AFTER STRIKE: " + darkArmy.getAll().get(j).toString());
                
                if (!isDead){
                    System.out.println("Dark army REVENGE!");
                    System.out.println("Attacker: " + darkArmy.getAll().get(j).toString());
                    System.out.println("BEFORE STRIKE: " + lightArmy.getAll().get(i).toString());
                    
                    lightArmy.attackCitizen(i, darkArmy.getAll().get(j).getPower());
                    
                    System.out.println("AFTER STRIKE: " + lightArmy.getAll().get(i).toString());
                }
            } else {
                System.out.println("Dark army attacks!");
                System.out.println("Attacker: " + darkArmy.getAll().get(j).toString());
                System.out.println("BEFORE STRIKE: " + lightArmy.getAll().get(i).toString());
                
                boolean isDead = lightArmy.attackCitizen(i, darkArmy.getAll().get(j).getPower());
                
                System.out.println("AFTER STRIKE: " + lightArmy.getAll().get(i).toString());
                
                if (!isDead){
                    System.out.println("Light army REVENGE!");
                    System.out.println("Attacker: " + lightArmy.getAll().get(i).toString());
                    System.out.println("BEFORE STRIKE: " + darkArmy.getAll().get(j).toString());
                        
                    darkArmy.attackCitizen(j, lightArmy.getAll().get(i).getPower());
                    
                    System.out.println("AFTER STRIKE: " + darkArmy.getAll().get(j).toString());
                }
            }
            
            System.out.println();
        }
        
        System.out.println("Light army after third round:");
        printArmy(lightArmy);
        System.out.println("Dark army after third round:");
        printArmy(darkArmy);
        
        if (lightArmy.isDead()){
            System.out.println("In final: DARK ARMY WINS");
            saveWonArmyState(darkArmy.getAll());
        } else {
            System.out.println("In final: LIGHT ARMY WINS");
            saveWonArmyState(lightArmy.getAll());
        }
    }
    
    public static void saveWonArmyState(ArrayList<MiddleEarthCitizen> army) throws FileNotFoundException, IOException{
        FileOutputStream fileOutputStream = new FileOutputStream("lastState.txt");
	BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
	ObjectOutputStream objectOutputStream = new ObjectOutputStream(bufferedOutputStream);
	objectOutputStream.writeObject(army);
	objectOutputStream.close();
    }
    
    public static ArrayList<MiddleEarthCitizen> loadWonArmyState() throws IOException, ClassNotFoundException{
        FileInputStream fileInputStream = new FileInputStream("lastState.txt");
	BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
	ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream);
	ArrayList<MiddleEarthCitizen> army = (ArrayList<MiddleEarthCitizen>)objectInputStream.readObject();
	objectInputStream.close();
	return army;
    }
    
    public static void printArmy(Army army){
        System.out.println("Riders:");
        for (int i=0;i<army.getRiders().size();i++){
            System.out.println(army.getRiders().get(i).toString());
        }
        
        System.out.println("Infantrymen:");
        for (int i=0;i<army.getInfantrymen().size();i++){
            System.out.println(army.getInfantrymen().get(i).toString());
        }
        
        System.out.println();
    }
    
    public static void printArmy(ArrayList<MiddleEarthCitizen> army){
        for (int i = 0; i < army.size(); i++){
            System.out.println(army.get(i).toString());
        }
        System.out.println();
    }
    
    public static boolean isLightArmyFirst(){
        Random r = new Random();
        return r.nextInt(2) == 0;
    }
    
    public static int randomizeIndex(ArrayList<MiddleEarthCitizen> citizens){
        Random r = new Random();
        int[] indexes = new int[citizens.size()];
        for (int i=0;i<citizens.size();i++){
            indexes[i] = -1;
        }
        
        int ind_size = 0;
        
        for (int i=0; i<citizens.size(); i++){
            if (!citizens.get(i).isDead()){
                indexes[ind_size] = i;
                ind_size++;
            }
        }
        return indexes[r.nextInt(ind_size)];
    }
}
