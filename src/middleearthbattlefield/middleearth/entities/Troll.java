/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class Troll extends MiddleEarthCitizen {
    protected int weight;
    
    public Troll(){
        generatePower();
    }
    
    public Troll(String n, int h, int w){
        super(n,h);
        weight = w;
        generatePower();
    }
    
    public void setWeight(int w){
        weight = w;
    }
    
    public int getWeight(){
        return weight;
    }
    
    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(5)+11;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Middle-earth-citizen", "Troll");
        return String.format("%s, weight = %d", oldStr, weight);
    }
}
