/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import middleearthbattlefield.middleearth.interfaces.IAnimal;
import java.util.Random;

/**
 *
 * @author User
 */
public class Horse implements IAnimal {
    protected String color;
    protected String breed;
    int power;
    
    public Horse(){
        generatePower();
    }
    
    public Horse(String c, String b){
        color = c;
        breed = b;
        generatePower();
    }
    
    public void setColor(String c){
        color = c;
    }
    
    public String getColor(){
        return color;
    }
    
    public void setBreed(String b){
        breed = b;
    }
    
    public String getBreed(){
        return breed;
    }

    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(2)+4;
    }

    @Override
    public int getPower() {
        return power;
    }
}
