/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class MiddleEarthCitizen implements Serializable {
    protected String name;
    protected int height;
    private static String origin = "MiddleEarth";
    protected int power;
    
    public MiddleEarthCitizen(){
        name = "";
        height = 0;
    }
    
    public MiddleEarthCitizen(String n, int h){
        name = n;
        height = h;
    }
    
    public void setName(String n){
        name = n;
    }
    
    public String getName(){
        return name;
    }
    
    public void setHeight(int h){
        height = h;
    }
    
    public int getHeight(){
        return height;
    }
    
    public static String getCountryOrigin(){
        return origin;
    }            
    
    public double heightToMeters(){
        return (double)height/100.0;
    }
    
    public String toString(){
        return String.format("Middle-earth-citizen: Name=%s, Power=%d, Height=%d", name, power, height);
    }
    
    public void generatePower(){
        power = 0;
    }
    
    public int getPower(){
        return power;
    }
    
    public boolean firstStrike(){
        return false;
    }
    
    public boolean isDead(){
        return power <= 0;
    }
    
    public void decreasePower(int p){
        power-=p;
    }
}
