/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class Wizard extends MiddleEarthCitizen {
    protected Horse horse;
    
    public Wizard(){
        generatePower();
    }
    
    public Wizard(String n, int h, Horse horse){
        super(n,h);
        this.horse = horse;
        generatePower();
    }
    
    public void setHorse(Horse h){
        horse = h;
    }
    
    public Horse getHorse(){
        return horse;
    }
    
    @Override
    public void generatePower() {
        power = 20 + horse.getPower();
    }
    
    @Override
    public boolean firstStrike(){
        return true;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Middle-earth-citizen", "Wizard");
        return oldStr;
    }
}
