/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class WoodenElf extends Elf {
    public WoodenElf(){
        generatePower();
    }
    
    public WoodenElf(String n, int h){
        super(n,h);
        generatePower();
    }
    
    @Override
    public void generatePower() {
        power = 6;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Elf", "Wooden elf");
        return oldStr;
    }
}
