/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import middleearthbattlefield.middleearth.interfaces.IAnimal;
import java.util.Random;

/**
 *
 * @author User
 */
public class Wolf implements IAnimal {

    protected String color;
    int power;
    
    public Wolf(){
        generatePower();
    }
    
    public Wolf(String c){
        color = c;
        generatePower();
    }

    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(4)+4;
    }

    @Override
    public int getPower() {
        return power;
    }
    
}
