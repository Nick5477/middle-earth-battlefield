/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class Human extends MiddleEarthCitizen {
    protected String homeTown;
    protected int age;
    
    public Human(){
        generatePower();
    }
    
    public Human(String town, int a, String n, int h){
        super(n,h);
        homeTown = town;
        age = a;        
        generatePower();
    }
    
    public Human(String town, int h){
        homeTown = town;
        height = h;
    }
    
    public void setHomeTown(String town){
        homeTown = town;
    }
    
    public String getHomeTown(){
        return homeTown;
    }
    
    public void setAge(int a){
        age = a;
    }
    
    public int getAge(){
        return age;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Middle-earth-citizen", "Human");
        return String.format("%s, Home town = %s, Age = %d", oldStr, homeTown, age);
    }
    
    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(2)+7;
    }
}
