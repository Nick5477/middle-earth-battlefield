/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class Orc extends MiddleEarthCitizen {
    private Wolf wolf;
    
    public Orc(){
        generatePower();
    }
    
    public Orc(String n, int h, Wolf w){
        super(n,h);
        wolf = w;
        generatePower();
    }
    
    public Orc(String n, int h){
        super(n,h);
        generatePower();
    }
    
    public final void setWolf(Wolf w){
        wolf = w;
    }
    
    public final Wolf getWolf(){
        return wolf;
    }
    
    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(3) + 8 + wolf.getPower();
    }
    
    @Override
    public boolean firstStrike(){
        return true;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Middle-earth-citizen", "Orc");
        return oldStr;
    }
}
