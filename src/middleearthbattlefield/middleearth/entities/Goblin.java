/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class Goblin extends MiddleEarthCitizen {
    public Goblin(){
        generatePower();
    }
    
    public Goblin(String n, int h){
        super(n,h);
        generatePower();
    }
    
    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(4) + 2;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Middle-earth-citizen", "Hoblin");
        return oldStr;
    }
}
