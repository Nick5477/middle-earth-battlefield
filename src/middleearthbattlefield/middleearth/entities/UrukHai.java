/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class UrukHai extends Orc {
    
    public UrukHai(){
        generatePower();
    }
    
    public UrukHai(String n, int h){
        super(n,h);
        generatePower();
    }
    
    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(3) + 10;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Orc", "UrukHai");
        return oldStr;
    }
}
