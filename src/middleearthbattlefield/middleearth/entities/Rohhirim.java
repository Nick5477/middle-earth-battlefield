/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.entities;

import java.util.Random;

/**
 *
 * @author User
 */
public class Rohhirim extends Human {
    
    protected Horse horse;
    
    public Rohhirim(String n, int a, Horse horse){
        super("Rohan", 170);
        name = n;
        age = a;
        this.horse = horse;
        generatePower();
    }
    
    public void setHorse(Horse h){
        horse = h;
    }
    
    public Horse getHorse(){
        return horse;
    }
    
    @Override
    public void generatePower() {
        Random r = new Random();
        power = r.nextInt(2)+7 + horse.getPower();
    }
    
    @Override
    public boolean firstStrike(){
        return true;
    }
    
    @Override
    public String toString(){
        String oldStr = super.toString();
        oldStr = oldStr.replaceAll("Human", "Rohhirim");
        return oldStr;
    }
}
