/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.armies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import middleearthbattlefield.middleearth.entities.Goblin;
import middleearthbattlefield.middleearth.entities.Orc;
import middleearthbattlefield.middleearth.entities.Troll;
import middleearthbattlefield.middleearth.entities.UrukHai;
import middleearthbattlefield.middleearth.entities.Wolf;

/**
 *
 * @author User
 */
public class DarkArmy extends Army implements Serializable {
    
    public DarkArmy(){
        riders = new ArrayList<>();
        infantrymen = new ArrayList<>();
        generateArmy();
    }    
    
    @Override
    public void generateArmy() {
        Random r = new Random();
        
        int orc_size = r.nextInt(ridersCount + 1);
        
        for (int i=0; i<orc_size; i++){
            Wolf riderWolf = new Wolf("Grey");
            riders.add(new Orc(String.valueOf(i), r.nextInt(131)+140, riderWolf));
        }

        int goblin_size = r.nextInt(infantrymenCount);
        
        for (int i=0;i < goblin_size; i++){
            infantrymen.add(new Goblin(String.valueOf(i), r.nextInt(21) + 150));
        }
        
        int urukhai_size = r.nextInt(infantrymenCount);
        
        for (int i=0;i <urukhai_size; i++){
            infantrymen.add(new UrukHai(String.valueOf(i), r.nextInt(41) + 190));
        }
        
        int troll_size = r.nextInt(infantrymenCount);
        
        for (int i=0;i < troll_size; i++){
            infantrymen.add(new Troll(String.valueOf(i), r.nextInt(31) + 250, r.nextInt(200) + 200));
        }
    }
    
}
