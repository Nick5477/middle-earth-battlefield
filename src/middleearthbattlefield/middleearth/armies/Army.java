/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.armies;

import java.util.ArrayList;
import middleearthbattlefield.middleearth.entities.MiddleEarthCitizen;

/**
 *
 * @author User
 */
public abstract class Army {
    public int ridersCount = 6;
    public int infantrymenCount = 6;
    
    ArrayList<MiddleEarthCitizen> riders;
    ArrayList<MiddleEarthCitizen> infantrymen;
    ArrayList<MiddleEarthCitizen> citizens;
    
    public void generateArmy(){
        
    }
    
    public ArrayList<MiddleEarthCitizen> getRiders(){
        return riders;
    }
    
    public ArrayList<MiddleEarthCitizen> getInfantrymen(){
        return infantrymen;
    }
    
    public boolean isRidersDead(){
        for (int i=0; i<riders.size(); i++){
            if (!riders.get(i).isDead()){
                return false;
            }
        }
        return true;
    }
    
    public boolean isInfantrymenDead(){
        for (int i=0; i<infantrymen.size(); i++){
            if (!infantrymen.get(i).isDead()){
                return false;
            }
        }
        return true;
    }
    
    public boolean isDead(){
        if (isRidersDead() && isInfantrymenDead()){
            return true;
        }
        else {
            return false;
        }
    }
    
    public ArrayList<MiddleEarthCitizen> getAll(){             
        return citizens;
    }
    
    public boolean attackRider(int i, int power){
        riders.get(i).decreasePower(power);
        return riders.get(i).isDead();
    }
    
    public boolean attackInfantrymen(int i, int power){
        infantrymen.get(i).decreasePower(power);
        return infantrymen.get(i).isDead();
    }
    
    public boolean attackCitizen(int i, int power){
        citizens.get(i).decreasePower(power);
        return citizens.get(i).isDead();
    }
    
    public void uniteAllCitizens(){
        citizens = new ArrayList<>();
        citizens.addAll(riders);
        citizens.addAll(infantrymen);
    }
}
