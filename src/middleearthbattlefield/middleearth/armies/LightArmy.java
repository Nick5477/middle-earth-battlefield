/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.armies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import middleearthbattlefield.middleearth.entities.Elf;
import middleearthbattlefield.middleearth.entities.Horse;
import middleearthbattlefield.middleearth.entities.Human;
import middleearthbattlefield.middleearth.entities.Rohhirim;
import middleearthbattlefield.middleearth.entities.Wizard;
import middleearthbattlefield.middleearth.entities.WoodenElf;


/**
 *
 * @author User
 */
public class LightArmy extends Army implements Serializable {

    public LightArmy(){
        riders = new ArrayList<>();
        infantrymen = new ArrayList<>();
        generateArmy();
    }
    
    @Override
    public void generateArmy() {
        Random r = new Random();
        
        if (r.nextInt(2) == 1){
            Horse wizardHorse = new Horse("Brown", "Auxois");
            riders.add(new Wizard("Hendalf", 180, wizardHorse));
        }
        
        int rohhirim_size = r.nextInt(ridersCount+1);
        
        for (int i=riders.size(); i<rohhirim_size; i++){
            Horse riderHorse = new Horse("Brown", "Auxois");
            riders.add(new Rohhirim(String.valueOf(i), r.nextInt(131)+20, riderHorse));
        }
        
        int wooden_elf_size = r.nextInt(infantrymenCount + 1);
        
        for (int i = 0;i < wooden_elf_size; i++){
            infantrymen.add(new WoodenElf(String.valueOf(i), r.nextInt(21) + 150));
        }
        
        int human_size = r.nextInt(infantrymenCount + 1);
        
        for (int i=0;i < human_size; i++){
            infantrymen.add(new Human("Aldburg", r.nextInt(51) + 20, String.valueOf(i), r.nextInt(41) + 150));
        }
        
        int elf_size = r.nextInt(infantrymenCount + 1);
        
        for (int i=0;i < elf_size; i++){
            infantrymen.add(new Elf(String.valueOf(i), r.nextInt(31) + 120));
        }
    }
    
}
