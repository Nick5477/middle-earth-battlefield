/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearthbattlefield.middleearth.interfaces;

import middleearthbattlefield.middleearth.entities.MiddleEarthCitizen;

/**
 *
 * @author User
 */
public interface MESSearchCriteria {
    boolean AcceptCriteria(MiddleEarthCitizen citizen);
}
